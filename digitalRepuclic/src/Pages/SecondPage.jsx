import React from 'react';
import { Footer, SecondForm } from '../Components';

function SecondPage() {
  return (
    <section className="page-container">
      <SecondForm />
      <Footer />
    </section>
  );
}

export default SecondPage;
