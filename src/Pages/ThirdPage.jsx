import React from 'react';
import { Footer, ThirdForm } from '../Components';

function ThirdPage() {
  return (
    <section className="page-container">
      <ThirdForm />
      <Footer />
    </section>
  );
}

export default ThirdPage;
