import React from 'react';
import { Footer, FourthForm } from '../Components';

function FourthPage() {
  return (
    <section className="page-container">
      <FourthForm />
      <Footer />
    </section>
  );
}

export default FourthPage;
